import jwt from 'jsonwebtoken';
import express from 'express';
import SKILLS from '../../skills.json';
const router = express.Router();

var USER = { 
  name: 'Andrew McCallum',
  email: 'amccallum89@gmail.com',
  password: 'password'
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/skills', function(req, res, next) {
  res.json(SKILLS.skills);
});

router.post('/authenticate', function(req, res, next) {

  // check if password matches
  if (USER.password !== req.body.password) {
    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
  } else {
    
    // we don't want to pass in the entire user since that has the password
    const payload = {
    	name: USER.name,
    	email: USER.email
    }

    // create a token with only our given payload
    var token = jwt.sign(payload, 'superSecret');

    // return the information including token as JSON
    res.json({
      success: true,
      message: 'Enjoy your token!',
      token: token
    });

  }

});

module.exports = router;
