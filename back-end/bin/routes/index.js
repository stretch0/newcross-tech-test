'use strict';

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _skills = require('../../skills.json');

var _skills2 = _interopRequireDefault(_skills);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

var USER = {
  name: 'Andrew McCallum',
  email: 'amccallum89@gmail.com',
  password: 'password'
};

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/skills', function (req, res, next) {
  res.json(_skills2.default.skills);
});

router.post('/authenticate', function (req, res, next) {

  // check if password matches
  if (USER.password !== req.body.password) {
    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
  } else {

    // we don't want to pass in the entire user since that has the password
    var payload = {
      name: USER.name,
      email: USER.email

      // create a token with only our given payload
    };var token = _jsonwebtoken2.default.sign(payload, 'superSecret');

    // return the information including token as JSON
    res.json({
      success: true,
      message: 'Enjoy your token!',
      token: token
    });
  }
});

module.exports = router;