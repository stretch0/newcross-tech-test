'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _skills = require('../../skills.json');

var _skills2 = _interopRequireDefault(_skills);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/* GET home page. */
router.post('/', function (req, res, next) {
  res.json('api');
});

module.exports = router;