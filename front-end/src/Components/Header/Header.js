import React, { Component } from 'react';
import './Header.css'


class Header extends Component {
 render() {
   return (
     <div className="header">
          
      <div className="selected">
        <span>1</span>
        <h6>General Information</h6>
      </div>

      <div>
        <span>2</span>
        <h6>Package Details</h6>
      </div>

    </div>
   );
 }
}
export default Header;