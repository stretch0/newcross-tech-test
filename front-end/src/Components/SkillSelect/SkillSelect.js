import React, { Component } from 'react';
import Select from 'react-select';
import './SkillSelect.css';
import 'react-select/dist/react-select.css';

class SkillSelect extends Component {

	constructor(props){
		super(props)

		this.state = {
			showFreeText: false,
			selectedOption: [],
			additionalSkills: '',
			inValid: this.props.inValid
		}

		this.timeout = null
	}

	componentDidUpdate(prevProps){
		if(this.props.inValid !== prevProps.inValid) this.setState({inValid: this.props.inValid})
	}

	addSelect = (e) => {
		e.preventDefault()
		this.setState({showFreeText: !this.state.showFreeText})
	}

	handleChange = (selectedOption) => {
		this.setState({ selectedOption });	 
  }

  handleInput = (e) => {
  	this.setState({additionalSkills: e.target.value})
  }

 	render() {

   	return (
     	<div className="skill-select">

     		<Select
	        name="form-field-name"
	        value={this.state.selectedOption}
	        multi={true}
	        placeholder="Select Skill Or Competency"
	        onChange={this.handleChange}
	        options={this.props.skills.map(skill => {return {value: skill, label: skill} })}
	      />
					
				<a className="add-free-text" onClick={this.addSelect}>{this.state.showFreeText ? '-' : '+'} Add Additional Skill</a>

				{
					this.state.showFreeText ?
						<textarea onChange={this.handleInput}/> : null
				}

			</div>
   	);
 	}
}
export default SkillSelect;