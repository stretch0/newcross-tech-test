import React, { Component } from 'react';
import './InfoCard.css'

class InfoCard extends Component {
 render() {
   return (
     <div className={`info-card-container `}>

     		<div>
	     		<h6>{this.props.card_title}</h6>
	     		<div className={`info-card ${this.props.className}`}>
	     			{ this.props.children }
	     		</div>
	     	</div>

     </div>
   );
 }
}
export default InfoCard;