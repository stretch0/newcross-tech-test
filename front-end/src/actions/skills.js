import { API_URL } from './../config';

export function fetchHasErrored(bool) {
  return {
    type: 'SKILLS_HAS_ERRORED',
    skillsHasErrored: bool
  };
}

export function fetchIsLoading(bool) {
  return {
    type: 'SKILLS_IS_LOADING',
    skillsIsLoading: bool
  };
}

export function fetchSuccess(skills) {
  return {
    type: 'SKILLS_SUCCESS',
    skills
  };
}

export function fetchSkills(jwt) {
  return (dispatch) => {
    dispatch(fetchHasErrored(true));

    fetch(`${API_URL}/skills?token=${jwt}`)
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        dispatch(fetchIsLoading(false));
        return response.json();
      })
      .then((response) => {
        dispatch(fetchSuccess(response))
      })
      .catch(() => {
        dispatch(fetchIsLoading(false))
        dispatch(fetchHasErrored(true))
      });
  };
}