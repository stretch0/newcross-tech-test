import { API_URL } from './../config';

export function authHasErrored(bool) {
  return {
    type: 'AUTH_HAS_ERRORED',
    authHasErrored: bool
  };
}

export function authIsLoading(bool) {
  return {
    type: 'AUTH_IS_LOADING',
    authIsLoading: bool
  };
}

export function authSuccess(jwt) {
  return {
    type: 'AUTH_SUCCESS',
    jwt
  };
}

export function authenticate() {
  return (dispatch) => {
    dispatch(authIsLoading(true));

    fetch(`${API_URL}/authenticate`, {
      method: 'POST',
      body: JSON.stringify({
        password: 'password'
      }),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
    .then((response) => {
      if (!response.ok) throw Error(response.statusText);

      dispatch(authIsLoading(false));
      return response.json();
    })
    .then((response) => {
      dispatch(authSuccess(response.token))
    })
    .catch(() => {
      dispatch(authIsLoading(false))
      dispatch(authHasErrored(true))
    });
  };
}