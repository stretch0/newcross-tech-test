import { combineReducers } from 'redux';
import * as auth from './auth';
import * as skills from './skills';
// import { posts, postsHasErrored, postsIsLoading } from './posts';


export default combineReducers({
	...auth,
	...skills
});