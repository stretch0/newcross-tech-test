export function authHasErrored(state = false, action) {
  switch (action.type) {
    case 'AUTH_HAS_ERRORED':
      return action.authHasErrored;

    default:
       return state;
  }
}

export function authIsLoading(state = false, action) {
  switch (action.type) {
    case 'AUTH_IS_LOADING':
      return action.authIsLoading;

    default:
      return state;
  }
}

export function jwt(state = [], action) {
  switch (action.type) {
    case 'AUTH_SUCCESS':
      return action.jwt;

    default:
      return state;
  }
}