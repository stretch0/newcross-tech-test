import React, { Component } from 'react';
import './General.css';

// Components
import InfoCard from '../../Components/InfoCard/InfoCard';
import SkillSelect from '../../Components/SkillSelect/SkillSelect';

class General extends Component {

	constructor(){
		super()

		this.state ={
			errors: {}
		}
	}

	handleSubmit(e){
		e.preventDefault()

		this.setState({errors: {}})

		const staff_gender_preferences = this.getValueOfRadioGroupd(document.getElementsByName("gender"))
		const end_date = this.getValueOfRadioGroupd(document.getElementsByName("end_date"))
		const skill_select = this.refs.skill_select.state;

		const form_data = {
			client_summary: this.refs.client_summary.value,
			duration_date: {
				start_data: this.refs.start_date.value,
				end_date: end_date
			},
			skills_competencies_required: {
				skils_competencies: skill_select.selectedOption.map(selected => selected.value),
				additional_skills: skill_select.additionalSkills,
			},
			staff_gender_preferences: staff_gender_preferences
		}

		this.validate(form_data)	
		console.log('SUBMITTED FORM DATA', form_data)
		
	}

	/*
	 *	Only validating skills field
	 **/
	validate(form_data){
		if(form_data.skills_competencies_required.skils_competencies.length < 1){

			// fetch any other errors that may be set on other fields and add this new error to the error object
			const allErrors = this.state.errors

			// add new error to existing object
			allErrors.skill_select = {
				errorMessage: 'You must select atleast one skill or competency'
			}

			// then setState with new error object 
			this.setState({errors: allErrors})	
			
		}
	}

	/**
	 * Helper function to get the selected value from a group of radio buttons
	 * Probably more of a utility function and doesn't belong here
	 **/
	getValueOfRadioGroupd(radioGroup){
		var selected;

		for(var i = 0; i < radioGroup.length; i++) {
			if(radioGroup[i].checked)
				selected = radioGroup[i].value;
		}

		return selected;
	}

 	render() {

   	return (

   		<form onSubmit={(e) => this.handleSubmit(e)} className="general">

   			{
   				/*
					 *	Because each Info Card has different form fields, I have made the component render it's child components.
					 *	This prevents having to pass in a bunch of props and have conditional statements within the component itself
					 **/
   			}
				<InfoCard card_title="Client Summary">
					<textarea ref="client_summary" placeholder="Please add summary of the client and why they need care..." />
				</InfoCard>

				<InfoCard card_title="Duration of Package">
					
					<div className="duration">
						<div className="start-date">
							<label htmlFor="date">Start Date</label>
							<input ref="start_date" id="date" type="date" />
						</div>

						<div className="end-date">
							<label>End Date</label>
							<div>
								<input defaultChecked="checked"  id="ongoing" type="radio" name="end_date" value="ongoing" />
								<label htmlFor="ongoing">Ongoing</label>
							</div>

							<div>
								<input id="specific" type="radio" name="end_date" value="specific" />
								<label htmlFor="specific">Specific</label>
							</div>
							
							
						</div>
					</div>

				</InfoCard>

				<InfoCard className="skills" card_title="Skills and Competencies Required">
					<SkillSelect ref="skill_select" skills={this.props.skills} />
					{ this.state.errors.skill_select ? <div>{this.state.errors.skill_select.errorMessage}</div> : null }
				</InfoCard>

				<InfoCard card_title="Staff Gender Preferences">
					<div className="gender-preferences">	
						<div>
							<input defaultChecked="checked"  id="none" type="radio" name="gender" value="none" />
							<label htmlFor="none">None</label>
						</div>
						
						<div>
							<input id="male" type="radio" name="gender" value="male" />
							<label htmlFor="male">Male</label>
						</div>

						<div>
							<input id="female" type="radio" name="gender" value="female" />
							<label htmlFor="female">Female</label>
						</div>
						
					</div>
				</InfoCard>

				<div className="submit-container">
					<button type="submit">Next</button>
				</div>

			</form>

   	);
 	}
}
export default General;