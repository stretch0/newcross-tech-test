import React, { Component } from 'react';
import {Switch, Route, withRouter} from 'react-router-dom';
import { connect } from 'react-redux'
import { authenticate } from '../../actions/authenticate'
import { fetchSkills } from '../../actions/skills'
import './App.css';

import General from '../General/General'
import Header from '../../Components/Header/Header'

class App extends Component {

  componentDidMount(){
    this.props.authenticate() // Authenticates every time app is loaded.
  }

  componentDidUpdate(prevProps){

    if(this.props.jwt && this.props.jwt !== prevProps.jwt ) this.props.fetchSkills(this.props.jwt) // If JWT is set, fetch skills list
  }

  render() {
    return (
      <div className="app">
        
        <Header />
        
        <div className="container">
          <Switch>  {/* I didn't need to use routng but have left it in */}
            <Route exact path='/' render={(props) => (
              <General {...props} skills={this.props.skills} />
            )}/> {/* calling the render method on the route so I can pass props down. Otherwise I would have to connect my General component to redux store as well and in this instance, was not necessary. */}
          </Switch>
        </div>
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authHasErrored: state.authHasErrored, // not using this state but would usually handle error or loading messages
    authIsLoading: state.authIsLoading, // not using this state but would usually handle error or loading messages
    jwt: state.jwt,
    skills: state.skills
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authenticate: () => dispatch(authenticate()),
    fetchSkills: (jwt) => dispatch(fetchSkills(jwt))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);


