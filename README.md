This repo includes both the front and back end

to run each app:

- front end:
    - yarn install
    - yarn start
    - defaults to http://localhost:3000

- back end:
    - yarn install
    - yarn build
    - yarn start
    - defaults to http://localhost:3037

I used node v6.11.3 in development so if you are having any issues running on your local, try using that node version.

